<?php

namespace App\Http\Controllers;

use App\Http\Requests\MailRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendMailController extends Controller
{
    public function submit(MailRequest $request) {
        $params = [
            'name' => $request->name,
            'phone' => $request->phone,
        ];
        Mail::send('mail', $params, function ($message)  use($request){
            $message->to('vlad.t.ysbm@gmail.com')->subject('Request from Y-Drive site'
            );
        });

        return response()->json(null, 200);
    }
}
