import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import VueNotifications from 'vue-notifications'

import '~/plugins'
import '~/components'

Vue.config.productionTip = false
const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
}
import miniToastr from 'mini-toastr'
miniToastr.init({types: toastTypes})
function toast ({title, message, type, timeout, cb}) {
  return miniToastr[type](message, title, timeout, cb)
}
const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}
Vue.use(VueNotifications, options)
/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
})
