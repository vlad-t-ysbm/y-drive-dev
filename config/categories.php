<?php
return [
    [
        'title_ru' => 'Легковое авто',
        'title_en' => 'Passenger car',
        'title_pl' => 'Samochód osobowy',
        'image' => '/img/car.png'
    ],
    [
        'title_ru' => 'Фургон',
        'title_en' => 'Van',
        'title_pl' => 'Wagon',
        'image' => '/img/car2.png'
    ]
];
