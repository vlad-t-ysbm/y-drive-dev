<?php
return [
    [
        [
            'title_ru' => 'Посуточная аренда',
            'title_en' => 'Daily rent',
            'title_pl' => 'Dzienny czynsz',
            'description_ru' => 'Детали предложения',
            'description_en' => 'Offer Details',
            'description_pl' => 'Szczegóły oferty',
            'price' => '000',
            'popular' => '0',
            'category_id' => '1',

        ],
        [
            'title_ru' => 'Понедельная аренда',
            'title_en' => 'Weekly rental',
            'title_pl' => 'Wynajem tygodniowy',
            'description_ru' => 'Детали предложения',
            'description_en' => 'Offer Details',
            'description_pl' => 'Szczegóły oferty',
            'price' => '000',
            'popular' => '1',
            'category_id' => '1',

        ],
        [
            'title_ru' => 'Долгосрочная аренда',
            'title_en' => 'Long term rental',
            'title_pl' => 'Wynajem długoterminowy',
            'description_ru' => 'Детали предложения',
            'description_en' => 'Offer Details',
            'description_pl' => 'Szczegóły oferty',
            'price' => '000',
            'popular' => '0',
            'category_id' => '1',

        ],
    ],
    [
        [
          'title_ru' => 'Почасовая аренда с водителем',
          'title_en' => 'Hourly rental with driver',
          'title_pl' => 'Wynajem na godziny z kierowcą',
          'description_ru' => 'Детали предложения',
          'description_en' => 'Offer Details',
          'description_pl' => 'Szczegóły oferty',
          'price' => '000',
          'popular' => '0',
          'category_id' => '1',
        ],
        [
          'title_ru' => 'Посуточная аренда',
          'title_en' => 'Daily rent',
          'title_pl' => 'Dzienny czynsz',
          'description_ru' => 'Детали предложения',
          'description_en' => 'Offer Details',
          'description_pl' => 'Szczegóły oferty',
          'price' => '000',
          'popular' => '0',
          'category_id' => '1',
        ],
        [
          'title_ru' => 'Долгосрочная аренда',
          'title_en' => 'Long term rental',
          'title_pl' => 'Wynajem długoterminowy',
          'description_ru' => 'Детали предложения',
          'description_en' => 'Offer Details',
          'description_pl' => 'Szczegóły oferty',
          'price' => '000',
          'popular' => '0',
          'category_id' => '1',
        ],
    ]
];
