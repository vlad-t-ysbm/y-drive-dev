<?php
return [
        [
            'image' => '/img/car.png',
            [
                'title_ru' => 'На газу',
                'title_en' => 'On gas',
                'title_pl' => 'Na gazie',
                'description_ru' => 'Стоимость топлива для вас будет в 2 раза дешевле',
                'description_en' => 'The cost of fuel for you will be 2 times cheaper',
                'description_pl' => 'Koszt paliwa będzie 2 razy tańszy',
                'category_id' => 1
            ],
            [
                'title_ru' => 'Дешево',
                'title_en' => 'Cheap',
                'title_pl' => 'Tanio',
                'description_ru' => 'У нас самые дешевые цены',
                'description_en' => 'We have the cheapest prices',
                'description_pl' => 'Mamy najtańsze ceny.',
                'category_id' => 1
            ],
            [
                'title_ru' => 'Комфорт',
                'title_en' => 'Comfort',
                'title_pl' => 'Wygoda',
                'description_ru' => 'Все авто оборудованы кондиционерами',
                'description_en' => 'All cars are air-conditioned',
                'description_pl' => 'Wszystkie samochody są klimatyzowane',
                'category_id' => 1
            ],
            [
                'title_ru' => 'Автокаско и техосмотр',
                'title_en' => 'Auto hull and vehicle inspection',
                'title_pl' => 'Automatyczna kontrola kadłuba i pojazdu',
                'description_ru' => 'Мы полностью покрываем расходы на страховку ОС,AC,NNW и техосмотр.',
                'description_en' => 'We fully cover the costs of OS, AC, NNW insurance and inspection.',
                'description_pl' => 'W pełni pokrywamy koszty ubezpieczenia i inspekcji OS, AC, NNW.',
                'category_id' => 1
            ],
            [
                'title_ru' => 'Экономичность',
                'title_en' => 'Cost-effectiveness',
                'title_pl' => 'Efektywność kosztowa',
                'description_ru' => 'Шкода Фабия имеет самый экономичный расход',
                'description_en' => 'Skoda Fabia has the most economical consumption',
                'description_pl' => 'Skoda Fabia ma najbardziej ekonomiczne zużycie',
                'category_id' => 1
            ],
            [
            'title_ru' => 'Быстро',
            'title_en' => 'Quickly',
            'title_pl' => 'Szybko',
            'description_ru' => 'Все оформление занимает от 30 минут',
            'description_en' => 'All registration takes 30 minutes',
            'description_pl' => 'Cała rejestracja zajmuje 30 minut',
            'category_id' => 1
            ],
        ],
        [
            'image' => '/img/car2.png',
            [
                'title_ru' => 'На газу',
                'title_en' => 'On gas',
                'title_pl' => 'Na gazie',
                'description_ru' => 'Стоимость топлива для вас будет в 2 раза дешевле',
                'description_en' => 'The cost of fuel for you will be 2 times cheaper',
                'description_pl' => 'Koszt paliwa będzie 2 razy tańszy',
                'category_id' => 1
            ],
            [
                'title_ru' => 'Дешево',
                'title_en' => 'Cheap',
                'title_pl' => 'Tanio',
                'description_ru' => 'У нас самые дешевые цены',
                'description_en' => 'We have the cheapest prices',
                'description_pl' => 'Mamy najtańsze ceny',
                'category_id' => 1
            ],
            [
                'title_ru' => 'Комфорт',
                'title_en' => 'Comfort',
                'title_pl' => 'Wygoda',
                'description_ru' => 'Все авто оборудованы кондиционерами',
                'description_en' => 'All cars are air-conditioned',
                'description_pl' => 'Wszystkie samochody są klimatyzowane',
                'category_id' => 1
            ],
            [
                'title_ru' => 'Автокаско и техосмотр',
                'title_en' => 'Auto hull and vehicle inspection',
                'title_pl' => 'Automatyczna kontrola kadłuba i pojazdu',
                'description_ru' => 'Мы полностью покрываем расходы на страховку ОС,AC,NNW и техосмотр.',
                'description_en' => 'We fully cover the costs of OS, AC, NNW insurance and inspection.',
                'description_pl' => 'W pełni pokrywamy koszty ubezpieczenia i inspekcji OS, AC, NNW.',
                'category_id' => 1
            ],
            [
                'title_ru' => 'Экономичность',
                'title_en' => 'Cost-effectiveness',
                'title_pl' => 'Efektywność kosztowa',
                'description_ru' => 'Шкода Фабия имеет самый экономичный расход',
                'description_en' => 'Skoda Fabia has the most economical consumption',
                'description_pl' => 'Skoda Fabia ma najbardziej ekonomiczne zużycie',
                'category_id' => 1
            ],
            [
                'title_ru' => 'Быстро',
                'title_en' => 'Quickly',
                'title_pl' => 'Szybko',
                'description_ru' => 'Все оформление занимает от 30 минут',
                'description_en' => 'All registration takes 30 minutes',
                'description_pl' => 'Cała rejestracja zajmuje 30 minut',
                'category_id' => 1
            ],
        ]
    ];
